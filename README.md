# Foxy Discord Bot

## Overview
Delivers pictures of Foxes and/or Kitsume from the relevant subreddits.

## Requirements to selfhost/dev
Requires:

* Node.js 12+
* Yarn
* Typescript
* Mongodb Instance
* Your own bot instance (details below)

Optional: 

* ts-node

## Discord bot instance
You will need to create your own bot instance

* Go to https://discordapp.com/developers/applications
* Create new application
* Bot tab on left hand side
* Copy the token

## Setup

1. Clone the repo
2. Copy the `.env.example` to `.env`
3. Fill in details, including teh mongodb info.
4. Use `yarn` to install the depenmdencies
5. Use `yarn dev` to start teh dev server of nodemon
    * Initial run sets up the collections, indexes and seeds data, this may take 10 min.
