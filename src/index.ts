import {Client, Intents, Message, PartialMessage,} from "discord.js";
import config from "./config/config"
import { liftDB } from "./config/db"
import { getPrefix, top_gg_functions } from "./JS/_functions"
import NodeCache from "node-cache"
import {Logger} from "./classes/Logger";
import {version} from "../package.json"

const logger = new Logger()
let start = new Date().getTime();

// commands
import { feed, help, prefixManager, random, remove } from "./commands/commands"

main().then(() => console.log("Complete")).catch(async(err) => await logger.discord(err));
async function main(){
  // https://github.com/discordjs/discord.js/issues/3924
    const client = new Client({
      intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES],
      partials: ['MESSAGE', 'CHANNEL'],
    });

    // for top.gg server count
    top_gg_functions(client)

    let {db} = await liftDB(true);

    // start teh cache
    const prefixes = new NodeCache();

    await client.login(config.token);

    client.once('ready', async() => {await logger.notify(`[${new Date().toISOString()}] [${config.env}] [v${version}] [${client.guilds.cache.size} guilds] [${(new Date().getTime()-start)/1000}s startup]`)});

    client.on('messageCreate', async (message) => {
      try {
        await manage_message(db, client, prefixes, message)
      } catch (e){
        console.error(e, new Error())
      }
    });
}

async function manage_message(db, client: Client, prefixes: NodeCache, message: Message | PartialMessage){
  if(message.partial){
    try {
      message = await message.fetch();
    } catch (error) {
      console.error('Something went wrong when fetching the message:', error);
      // Return as `reaction.message.author` may be undefined/null
      return;
    }
  }

  // bot ignores other bots and itself
  if(message.author.bot){ return }
  // no dm support
  if(message.channel.type === "DM"){ return }

  if(client.user === null){ return }

  // get the channel's prefix
  let prefix = await getPrefix(message, prefixes, db, client.user.id)

  // every command has to have the prefix
  if (!message.content.startsWith(prefix)){ return }

  const args = message.content.slice(prefix.length).trim().toLowerCase().split(/ +/g);

  // args[0] is teh command
  if (args[0] === "feed") {
    return await feed(message, prefixes, db ,args);
  }
  if(args[0] === "help"){
    return await help(message, prefixes, client.user.id);
  }
  if (args[0] === "random") {
    return await random(message, prefixes, db, args);
  }
  if (args[0] === "prefix") {
    return await prefixManager(message, prefixes, db, args);
  }
  if (args[0] === "remove") {
    return await remove(message, prefixes, db ,args);
  }

  // catch the end of it
  return await help(message, prefixes, client.user.id);
}