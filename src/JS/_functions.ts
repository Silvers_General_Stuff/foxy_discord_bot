import {getItems} from "../config/db";
import {Client, Intents, MessageEmbed, MessageOptions, TextChannel} from "discord.js";
import config from "../config/config";
import {Details, Post, Servers} from "./_structures";
import DBL from "dblapi.js"
import {Logger} from "../classes/Logger";

const logger = new Logger()

export const convertArrayToKeyedObject = (array: Array<object>, key:string = "id"): object => {
    return Object.assign({},...array.map(item => ({[item[key]]: item})))
};

export async function sendMessages(messages: Array<any>, type:string){
  const client = new Client({
    intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES],
    partials: ['MESSAGE', 'CHANNEL'],
  });
    await client.login(config.token);

    client.on('ready', async () => {
        let sending_messages = messages
            .map(async(message) => {
                // check if the channel can take a text message
                let channel = await client[type].cache.get(message.channel);
                if(typeof channel !== "undefined"){
                    if(
                        channel.type === "text" ||
                        channel.type === "dm" ||
                        channel.type === "news"
                    ){
                        let channelTemp = channel as TextChannel;
                        await channelTemp.send({embeds: [message.message]})
                    }
                }
            });

        await Promise.all(sending_messages);

        client.destroy();
    });
}

export const filterRedditResults = async(posts:Array<Post>, existingItems:object, limit:number = 5):Promise<Array<Post>> =>{
    let now = new Date().getTime();
    let firstFilters =  posts
        // filter out stickied posts
        .filter(post => !post.data.stickied)

        // after the stickied posts are filtered out apply the limit then
        .slice(0, limit)

        // filter out text posts
        .filter(post => post.data.selftext === "")

        // filter out self posts
        .filter(post => !post.data.is_self)

        // filter out NSFW posts
        .filter(post => !post.data.over_18)

        // filter out existing posts
        .filter(post => !existingItems[post.data.id])

        // filter out all posts younger than an hour
        .filter(post => ( now - post.data.created) > 3600)

    // process Gfycat images, using teh data that reddit captures itself
    let processed = firstFilters.map(processGfycat);

    let resolved  = await Promise.all(processed)

        // filter out non image/gif posts
    return resolved.filter(post => post.data.url.match(/\.(jpeg|jpg|png|gif|gifv|mp4|webm)$/) != null)

        // filter out v.redd.it posts
        .filter(post => post.data.url.indexOf("v.redd.it") === -1)

};

const processGfycat =(post:Post)=>{
    // return all non gfycat posts
    if(post.data.url.indexOf("gfycat.com") === -1){return post}
    if(!post.data.secure_media){return post}

    //let newUrl = post.data.secure_media.oembed.thumbnail_url
    //newUrl = newUrl.replace("-size_restricted.gif", ".mp4")
    //newUrl = newUrl.replace("thumbs.gfycat.com", "giant.gfycat.com")

    post.data.url = post.data.secure_media.oembed.thumbnail_url

    return post
}

export const processMessages = async (db, details:Array<Details>, type:string): Promise<{ channel: string, message: MessageOptions }[]> =>{
    let messages: {channel: string, message: MessageOptions}[] = [];

    // get all teh servers
    let servers = await getItems(db, "servers", {active:true}).catch(async(err) => {err.misc="servers";await logger.discord(err);return []}) as Array<Servers>;

    // loop through each
    for(let i=0;i<servers.length;i++){
        // if the server idoes not wish to see this content it passes over them
        if(
            // if they chose the default then this wont be envoked
            servers[i].dataType !== "all" &&
            // if the items type isnt in the itemtype string of the request then it skips it
            servers[i].dataType.indexOf(type) === -1
        ){continue}

        // check if we are sending a message to the channel based on teh config
        for(let j=0;j<details.length;j++){
          let message: MessageOptions = {};
            let embed = new MessageEmbed();
            embed.setTitle(details[j].name);
            embed.setURL(`https://reddit.com/r/${type}/comments/${details[j].id}`)
            embed.setDescription(`/r/${type}`)

            if(details[j].url.match(/\.(jpeg|jpg|png|gif)$/) != null){
                embed.setImage(details[j].url);
            }else{
                message.files = [details[j].url];
            }

            message.embeds = [embed];

            messages.push({channel:servers[i].channel, message:message});
        }
    }

    return messages
};

export const getPrefix = async (message, prefixes, db, botID:string): Promise<string> =>{
    // prefix can be a mention or the actual set prefix

    // first check if it is a mention
    if(message.content.startsWith(`<@!${botID}>`)){
        return `<@!${botID}>`
    }

    let guild = message.guild.id.toString();

    // check if its already in teh prefix cache
    let cached = prefixes.get(guild)
    if(typeof cached !== "undefined"){
        return cached
    }

    // if it is not a mention of the bot
    // else check if the guild/server exists
    let id = `${guild}_${message.channel.id.toString()}`;
    let server = await getItems(db,"servers", {id:id}, {prefix:1}).catch(async(err) => {err.misc="servers";await logger.discord(err);return []}) as Array<Servers>;

    // if it dosent return teh default preset from the env
    if(server.length === 0){
        // set it in teh cache for future use
        prefixes.set(guild, config.prefix)
        return config.prefix
    }

    // if it does then return teh prefix attached
    // only care about one channel as teh change prefix command will change it for all channels
    prefixes.set(guild, server[0].prefix)
    return server[0].prefix
};

export const top_gg_functions = (client) =>{
    // just in case tehre is nothing here
    if(config.top_gg === ""){return}

    const dbl = new DBL(config.top_gg, client);

    dbl.on('posted', () => {
        console.log('Server count posted!');
    })

    dbl.on('error', e => {
        console.log(`Oops! ${e}`);
    })
}