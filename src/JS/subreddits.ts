import {liftDB, addItemBulk, dataToDbArray, getItems} from "../config/db";
import config from "../config/config";
import { sendMessages, processMessages, filterRedditResults, convertArrayToKeyedObject} from "./_functions";
import {Details, Post} from "./_structures";
import {Logger} from "../classes/Logger";
import axios from "axios";
const logger = new Logger()

main().then(() => console.log("Complete")).catch(async(err) => await logger.discord(err));
async function main(){
    console.time(`Total`);
    // connects to DB
    let {db,client} = await liftDB();

    let subReddits = config.subReddits;

    for(let i=0;i<subReddits.length;i++){
        console.time(`SubReddit: ${subReddits[i]}`);
        await processSubreddit(db, subReddits[i])
        console.timeEnd(`SubReddit: ${subReddits[i]}`);
    }

    client.close()

    console.timeEnd(`Total`);
}

const processSubreddit = async(db, subReddit:string) =>{
    // gets keyed object of existing subReddit
    let existingItems = await getExistingItems(db, subReddit);

    // gets list of new items
    let currentItems = await axios.get(`https://api.reddit.com/r/${subReddit}`)
        // gets teh data from teh response object
        .then(result => result.data)
        // gets list of posts
        .then(result => result.data.children)
        .catch(async(error) => {
            error.misc=error.config.url;
            if (error.response.status !== 503){
                await logger.discord(error);
            }
            return []
        }) as Array<Post>

    // filter out self, existing and nsfw posts
    let filtered = await filterRedditResults(currentItems, existingItems)

    console.log(filtered)
    // ends it here if there is nothing
    if(filtered.length === 0){ return }

    // put into a nice format
    let processed:Array<Details> = filtered.map(post => {return {id: post.data.id, name: post.data.title, url:post.data.url}})

    // logs to db
    let dbArray = dataToDbArray(processed, 25000, "id", { firstAdded: new Date().toISOString() });
    await addItemBulk (db, subReddit,  dbArray);

    // then send details to the specified channels
    let messages = await processMessages(db, processed, subReddit);

    // send the messages

  try {
    await sendMessages(messages, "channels");
  } catch (e){
    console.error(e, new Error())
  }

}

async function getExistingItems(db, collection):Promise<object>{
    let items = await getItems(db, collection,{}, {id:1}).catch(async(err) => {err.misc=collection;await logger.discord(err);return []}) as Array<Details>;
    return convertArrayToKeyedObject(items);
}