export interface Post {
    data: PostData;
}

export interface PostData {
    created: number;
    selftext: string;
    over_18: boolean;
    is_self: boolean;
    stickied: boolean;
    id: string;
    url: string;
    title: string;
    secure_media: secure_media;
    subreddit: string
}
interface secure_media{
    oembed: secure_media_oembed
}

interface secure_media_oembed{
    thumbnail_url: string;
}

export interface Servers {
    id: string;
    guild: string;
    channel: string;
    user: string;
    dataType: string;
    active: boolean;
    prefix: string;
}

export interface Details {
    id: string;
    name: string;
    url: string;

}

export class CustomError extends Error{
    misc?: string
}