import * as Mongo from "mongodb"
import config from "./config";
import {indexes, collections} from "./db_setup";
const mongodb = Mongo.MongoClient;
import {Logger} from "../classes/Logger";
const logger = new Logger()

export async function liftDB (index:boolean=false){
    console.time("Connecting to Database");
    let client = await mongodb.connect(config.db.uri, { useNewUrlParser: true, useUnifiedTopology: true }).catch(async(err) => {err.misc = "MongoDB failed to connect, exiting"; await logger.discord(err); process.exit(1) });
    let db = client.db(config.db.database);
    console.timeEnd("Connecting to Database");

    // these only run if the index.ts is run
    if(index){
        console.time("Creating: Collections");
        await collections(db, config.subReddits);
        console.timeEnd("Creating: Collections");

        console.time("Creating: Indexes");
        await indexes(db, config.subReddits);
        console.timeEnd("Creating: Indexes");
    }

    return {db, client}
}

export async function addItem(db, collection:string, item:object, field:string = "id"){
    let find = {};
    find[field] = item[field];
    return await db.collection(collection).updateOne(find, {$set: item}, {upsert: true})
}

export async function addItemBulk (db, collection: string,  dbArray, location:Error = new Error()){
    for (let i = 0; i < dbArray.arrays + 1; i++) {
        if (dbArray[i].length > 0) {
            await db.collection(collection).bulkWrite(dbArray[i], { ordered: false }).catch(async(err) => {err.misc = collection;await logger.discord(err,location)})
        }
    }
}

export function dataToDbArray (data:Array<object>, limit:number= 25000, accessor:string = "id", setOnInsert?:object) {
    let tmp = { arrays: 0, 0: [] };
    for (let j = 0; j < data.length; j++) {
        let temp = {
            updateOne: {
                filter:{},
                update: {
                    $set: data[j]
                },
                upsert: true
            }
        };
        temp.updateOne.filter[accessor] = data[j][accessor];
        if (setOnInsert) {
            temp.updateOne.update["$setOnInsert"] = setOnInsert
        }
        if (tmp[tmp.arrays].length < limit) {
            tmp[tmp.arrays].push(temp)
        } else {
            tmp.arrays += 1;
            tmp[tmp.arrays] = [temp]
        }
    }
    return tmp
}


export async function deleteItem(db, collection:string, item:string, field:string = "id"){
    let find = {};
    find[field] = item;
    return await db.collection(collection).deleteOne(find);
}

export async function getItems(db, collection:string,find:object = {}, project:object = {_id:0}):Promise<Array<object>>{
    return db.collection(collection).find(find).project(project).toArray();
}