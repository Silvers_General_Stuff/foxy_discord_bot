import dotenv from 'dotenv'
import packageData from '../../package.json'

dotenv.config();


// database setup
const database = process.env.DB;
const dbPort = process.env.DB_PORT || 27017;
const dbLocation = process.env.DB_LOCATION || "localhost";
const dbUser = process.env.DB_USER;
const dbPass = process.env.DB_PASS;
let dbURL = 'mongodb://' + dbUser + ':' + dbPass + '@' + dbLocation + ':' + dbPort + '/admin';
if (dbUser === '' || dbPass === '') {
  dbURL = 'mongodb://' + dbLocation + ':' + dbPort
}

if (dbLocation.indexOf(".") !== -1) {
  dbURL = 'mongodb+srv://' + dbUser + ':' + dbPass + '@' + dbLocation + '/admin?ssl=false'
}

const subreddits = process.env.SUBREDDITS || "foxes"

const config = {
  name: packageData.name,
  version: packageData.version,
  env: process.env.ENVIROMENT || "prod",
  db: {
    uri: dbURL,
    database: database
  },
  token:process.env.TOKEN,
  prefix: process.env.PREFIX || "-",
  subReddits: subreddits.split(","),
  loggingWebhook: process.env.LOGGING_WEBHOOK || "",
  top_gg : process.env.TOP_GG || ""
};

export default config
