
// this creates teh collections
export const collections = async (db, subReddits:Array<string>)=>{
    await db.createCollection("servers");

    // create collections for each subreddit
    subReddits.forEach(async (subreddit:string) => await db.createCollection(subreddit))

};

// this creates teh indexes
export const indexes = async (db, subReddits:Array<string>)=>{
    await db.collection('servers').createIndex({ 'id': 1 });
    await db.collection('servers').createIndex({ 'active': 1 });
    await db.collection('servers').createIndex({ 'guild': 1 });

    // create an id index for each subreddit
    subReddits.forEach(async (subreddit: string) => await db.collection(subreddit).createIndex({ 'id': 1 }))

};

