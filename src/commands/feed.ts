import config from "../config/config";
import {addItem, getItems} from "../config/db";
import {Servers} from "../JS/_structures";
import {Logger} from "../classes/Logger";
const logger = new Logger()

export const feed = async(message, prefixes, db ,args) =>{
    // check if user has MANAGE_CHANNELS permission
    if(!message.channel.permissionsFor(message.member).has("MANAGE_CHANNELS")){
        return message.channel.send(`User access not high enough, requires MANAGE_CHANNELS permission`)
    }
    let guild = message.guild.id.toString();
    let id = `${guild}_${message.channel.id.toString()}`;
    let prefix =  prefixes.get(guild) || config.prefix;

    let helpText = `
This is the feed command, it displays a feed of images from the specified subreddits, default one is /r/foxes.
Only images marked SWF are shown

Commands:
\`\`${prefix}${args[0]} help\`\`: This help text
\`\`${prefix}${args[0]} types\`\`: Displays list of available subreddits
\`\`${prefix}${args[0]} start\`\`: Starts the feed in this channel with a default subreddit of "foxes"
\`\`${prefix}${args[0]} start [types]\`\`: can specify a comma seperated lsit of subreddits
    Example: \`\`${prefix}${args[0]} start foxes,awoo,kitsunemimi\`\`
    Example: \`\`${prefix}${args[0]} start all\`\`
\`\`${prefix}${args[0]} stop\`\`: Stops the feed in this channel.
\`\`${prefix}${args[0]} status\`\`: Lists what is being returned for teh channel.
`;


    // if no argument is specified or help sepcifically called
    if(typeof args[1] === "undefined" || args[1] === "help"){
        return message.channel.send(helpText)
    }

    if(args[1] === "types"){
        return message.channel.send(`Available Types:\n${config.subReddits.join("\n")}`)
    }

    if(args[1] === "start"){
        let item:Servers = {
            id: id,
            guild: guild,
            channel: message.channel.id.toString(),
            user: message.author.id.toString(),
            dataType: args[2] || "foxes",
            active: true,
            prefix: prefix
        };

        await addItem(db, "servers", item).catch(async(err) => {err.misc=`addItem. User: ${message.author.id.toString()}`;await logger.discord(err)});
        return message.channel.send("Added")
    }

    if(args[1] === "stop"){
        let item:Partial<Servers> = {
            id: id,
            active: false
        };
        await addItem(db, "servers", item).catch(async(err) => {err.misc=`addItem. User: ${message.author.id.toString()}`;await logger.discord(err)});
        return message.channel.send("Removed")
    }

    // find the channel info
    if(args[1] === "status"){
        let serverData = await getItems(db, "servers", {id:id}).catch(async(err) => {err.misc="servers";await logger.discord(err);return []}) as Array<Servers>;
        if(serverData.length === 0 ){
            return message.channel.send(`Channel not set up, please run \`\`${prefix}feed\`\` to start`)
        }

        let active = serverData[0].dataType
        // show that it is all
        if(active === "all"){
            active = `all: ${config.subReddits.join(",")}`
        }
        return message.channel.send(`Selected options: ${active}`)
    }

    // if it falls to here then its not picked up with any of the other commands, shoe help text
    return message.channel.send(helpText)
};