import config from "../config/config";
import { deleteItem } from "../config/db";
import {Logger} from "../classes/Logger";
const logger = new Logger()

export const remove = async(message, prefixes, db ,args) =>{
    // check if user is me
    if(message.author.id !== "136522490632601600"){
        return message.channel.send(`User access not high enough, requires BOT_OWNER permission`)
    }

    let guild = message.guild.id.toString();
    let prefix =  prefixes.get(guild) || config.prefix;

    let helpText = `
This is the remove command, it removes images from teh database, just need the reddit link like: 
\`\`https://reddit.com/r/kitsunemimi/g9u3f2\`\`
\`\`https://www.reddit.com/r/kitsunemimi/comments/g9u3f2/kitsume_sanic/\`\`

Commands:
\`\`${prefix}${args[0]} help\`\`: This help text
\`\`${prefix}${args[0]} [reddit link]\`\`: reddit link to remove
`;


    // if no argument is specified or help sepcifically called
    if(typeof args[1] === "undefined" || args[1] === "help"){
        return message.channel.send(helpText)
    }

    let subreddit, redditID;
    let split = args[1].split("/")
    if(split.length < 6){return message.channel.send(`Invalid link`)}
    switch(split.length){
        case 6: {
            subreddit = split[4];
            redditID = split[5];
            break
        }
        default: {
            subreddit = split[4];
            redditID = split[6];
            break
        }
    }

    await deleteItem(db, subreddit, redditID).catch(async(err) => {err.misc=`Removing ${subreddit}_${redditID}`;await logger.discord(err);return});

    return message.channel.send(`Deleted ${redditID} from /r/${subreddit}`)
};