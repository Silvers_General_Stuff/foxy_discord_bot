import config from "../config/config";
import { getItems, dataToDbArray, addItemBulk} from "../config/db";
import {Servers} from "../JS/_structures";
import {Logger} from "../classes/Logger";
const logger = new Logger()

export const prefixManager = async(message, prefixes, db, args) =>{
    // check if user has MANAGE_CHANNELS permission
    let permission = "MANAGE_GUILD"
    if(!message.channel.permissionsFor(message.member).has(permission)){
        return message.channel.send(`User access not high enough, requires ${permission} permission`)
    }
    let guild = message.guild.id.toString();
    let id = `${guild}_${message.channel.id.toString()}`;

    let prefix =  prefixes.get(guild) || config.prefix;

    let helpText = `
This is the prefix command, it changes teh prefix for this server

Commands:
\`\`${prefix}${args[0]} help\`\`: This help text
\`\`${prefix}${args[0]} \`\`: resets prefix to ${config.prefix}
\`\`${prefix}${args[0]} [prefix]]\`\`: Starts the feed in this channel with a default subreddit of "foxes"
    Example: \`\`${prefix}${args[0]} !\`\`
    Example: \`\`${prefix}${args[0]} CallTheFoxBot\`\`
`;


    // this is basically the only "prefix" that is reserved
    if(args[1] === "help"){
        return message.channel.send(helpText)
    }

    let newPrefix = args[1] || config.prefix;

    // get all the entries for this server
    let servers = await getItems(db, "servers", {guild:guild}, {id:1}).catch(async(err) => {err.misc="servers";await logger.discord(err);return []}) as Array<Servers>;
    let updated:Partial<Servers>[] = []
    for(let i=0;i<servers.length;i++){
        let item:Partial<Servers> = {
            id: servers[i].id,
            prefix: newPrefix
        }
        updated.push(item)
    }
    // just inc ase this is the fist thing the user does
    if(updated.length===0){
        updated.push({
            id: id,
            prefix: newPrefix
        })
    }

    // set in cache
    prefixes.set(guild, newPrefix)
    // tehn server
    let forDB = dataToDbArray(updated, 25000, "id", { firstAdded: new Date().toISOString() })
    await addItemBulk (db, "servers",  forDB);
    // then return message back to teh user
    return message.channel.send(`Set prefix to ${newPrefix}`)
};