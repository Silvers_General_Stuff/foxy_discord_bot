import config from "../config/config";

export const help = async(message, prefixes, botID) => {
    let guild = message.guild.id.toString();
    let prefix =  prefixes.get(guild) || config.prefix;
    ``
    let text = `
Default prefix is ${config.prefix}, mentioning the bot also works. 
    Example: <@!${botID}> help
This shows the help commands available

Channel Commands: 
${prefix}help: Displays this message
${prefix}feed: Shows the help text for the "feed" command (MANAGE_CHANNELS permission required)
${prefix}random help: Shows the help text for the "random" command
${prefix}prefix help: Shows the help text for the "prefix" command (MANAGE_GUILD permission required)

`;

    return message.channel.send(text)
};