import config from "../config/config";
import { getItems} from "../config/db";
import { MessageEmbed } from "discord.js";
import { Servers, Details } from "../JS/_structures";
import { Logger } from "../classes/Logger";

export const random = async(message, prefixes, db, args) =>{
    const logger = new Logger()
    let guild = message.guild.id.toString();
    let id = `${guild}_${message.channel.id.toString()}`;
    let prefix =  prefixes.get(guild) || config.prefix;

    let helpText = `
Shows a randomised image based on the channels settings.

Commands:
\`\`${prefix}${args[0]} \`\`: Returns a randomised images from the archives based on the channels settings
\`\`${prefix}${args[0]} help\`\`: This help text
\`\`${prefix}${args[0]} active\`\`: Shows what types teh images are pulled from.
\`\`${prefix}${args[0]} [type]\`\`: Shows a random image from teh specified collection

`;

    // Only show the help if specifically called
    // no need to call the DB for this
    if(typeof args[1] !== "undefined" && args[1] === "help"){
        return message.channel.send(helpText)
    }

    // find the channel info
    let serverData = await getItems(db, "servers", {id:id}).catch(async(err) => {err.misc="servers";await logger.discord(err);return []}) as Array<Servers>;

    let active
    // server isnt set up
    if(serverData.length ===0){
        active = "foxes"
    }else{
        active = serverData[0].dataType
    }

    // Only show the help if specifically called
    if(typeof args[1] !== "undefined" && args[1] === "active"){
        // show that it is all
        if(active === "all"){
            active = `all: ${config.subReddits.join(",")}`
        }
        return message.channel.send(`Selected options: ${active}`)
    }

    if(active === "all"){active = config.subReddits.join(",")}

    // get a random category
    let collection = getRandomFromArray(active.split(","))

    // if there is an argument that is not one of the ones abve
    if(typeof args[1] !== "undefined"){
        // check if it is one of the specified arguments

        // set the base to be what specified on teh server
        let baseTypes:string = serverData[0].dataType

        // if all is what is specified swap it out for all teh items from teh config
        if(serverData[0].dataType === "all"){
            baseTypes = config.subReddits.join(",")
        }
        if(baseTypes.indexOf(args[1]) !== -1){
            collection = args[1]
        }else{
            // if its not p[art of the ones set for teh channel, or just random stuff
            await message.channel.send(`No images available for ${args[1]}, using ${collection}`)
        }
    }

    // get total number of items
    let total = await db.collection(collection).countDocuments();
    // get random with taht upper limit
    // floor brings it down
    let random = Math.floor(Math.random() * (total - 0));

    // sample getrs a random sample of size 1
    let result = await db.collection(collection).find({}).skip(random).limit(1).toArray().catch(async(err) => {err.misc=collection;await logger.discord(err);return []}) as Array<Details>;

    if(result.length ===0){
        return message.channel.send(`No images available for ${collection}`)
    }

    let embed = new MessageEmbed();
    embed.setTitle(result[0].name);
    embed.setImage(result[0].url);
    embed.setURL(`https://reddit.com/r/${collection}/${result[0].id}`)
    embed.setDescription(`/r/${collection}`)


    // send the image
    return message.channel.send({embeds: [embed]})
};


const getRandomFromArray = (array:Array<any>) => {
    return array[Math.floor((Math.random()*array.length))];
}