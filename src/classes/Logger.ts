import path from "path";
import config from "../config/config";
import { CustomError } from "../JS/_structures";
import axios from "axios";

export class Logger {
    // set up attributes
    private readonly pathname: string;
    private webhook: string;
    private useConsole: boolean;
    private message: Array<string>
    private location: Error;


    constructor() {
        // set up basic info

        this.pathname = path.resolve(".")
            // to deal with windows paths fecking it all up
            .replace(/\\/g, "\\\\");

        if (config.loggingWebhook === "") {
            this.useConsole = true
        } else {
            this.useConsole = false
            this.webhook = config.loggingWebhook
        }

        this.message = []
    }

    console(error: Error, secondError?: Error, location: Error = new Error()) {
        this.location = location

        if (secondError) {
            console.error(secondError)
        }
        console.error(error)
        console.error(this.location)
        return
    }

    private processMessage(error: Error) {
        // if there is a specific error message
        if ("misc" in error) {
          let tmp = error as CustomError
          if (tmp.misc != null) {
            this.message.push('`', tmp.misc, '`')
          }
        }
        // this is where we trace back to the source of the issue
        if (error.stack && this.location && this.location.stack) {
            // replace all instances of the path
            error.stack = error.stack.replace(new RegExp(this.pathname, "gi"), '.');
            this.location.stack = this.location.stack.replace(new RegExp(this.pathname, "gi"), '.');

            // the actual error message does not contain teh actual location of teh issue, just what module caused it
            // so I am using the location var with is a new error() and stripping out what I want from taht and merging teh two together

            // split both into arrays
            let errorArray = error.stack.split("\n");
            let locationArray = this.location.stack.split("\n")

            let newError = [
                // first line from the error
                errorArray[0],
                // third line from teh location
                locationArray[2],
                // rest of the error
                ...errorArray.splice(1)
            ];

            this.message.push('```js', newError.join("\n"), '```')
        }
    }

    async discord(error: Error, location: Error = new Error()) {
        this.location = location

        this.processMessage(error)

        await this.useWebhook(this.message.join('\n'))

        return
    }

    // falls back to console if this fails
    private async useWebhook ( content,error= new Error()){
        await axios
            .post(this.webhook, {content: content})
            // if webhook failes it falls back on printing to console
            .catch((err) => this.console(error, err))

    }

    async notify(message:string){
        await this.useWebhook(message)
    }

}