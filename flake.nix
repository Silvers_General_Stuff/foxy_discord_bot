{
  description = "Foxy Discord Bot";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-22.05";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils, ... }: utils.lib.eachDefaultSystem (system:
    let

      pkgs = import nixpkgs {
        inherit system;
        overlays = [ ];
      };

      package_name = "discord_foxy";
      # name in teh package.json
      package_name_alt = "foxy_discord_bot";

      description = "Foxy Discord Bot";

      # set the node version here
      nodejs = pkgs.pkgs.nodejs-16_x;

    in rec {

      packages."${package_name}" = let
          original = pkgs.mkYarnPackage {
            name = "${package_name}";
            buildInputs = [
              nodejs
            ];
            src = ./.;
            packageJSON = "${./package.json}";
            yarnLock = "${./yarn.lock}";
            buildPhase = "yarn build";
          };
        # takes teh outpout and makes it nicer
        in pkgs.stdenv.mkDerivation {
          name = "${package_name}";
          src = original;
          installPhase = ''
            mkdir -p $out
            cp -R $src/libexec/${package_name_alt}/deps/${package_name_alt}/. $out
            rm $out/node_modules
            cp -R $src/libexec/${package_name_alt}/node_modules/. $out/node_modules
          '';
        };


      defaultPackage = packages."${package_name}";

      nixosModule = { lib, pkgs, config, ... }:
        with lib;
        let
          cfg = config.services."${package_name}";

          service_name = script: lib.strings.sanitizeDerivationName("${cfg.prefix}${cfg.user}@${script}");

          root_dir = self.defaultPackage."${system}";

          # oneshot scripts to run
          serviceGenerator = mapAttrs' (script: time: nameValuePair (service_name script) {
            description = "${description} ${script}";

            wantedBy = [ ];
            after = [ "network-online.target" ];
            serviceConfig = {
              Type = "oneshot";
              DynamicUser=true;
              ExecStart = "${nodejs}/bin/node ${root_dir}${scripts_path}${script}";
              WorkingDirectory="${root_dir}";
              EnvironmentFile = "${cfg.config}";
            };
          });

          # each timer will run the above service
          timerGenerator = mapAttrs' (script: time: nameValuePair (service_name script) {
            description="${description} ${script}";

            wantedBy = [ "timers.target" ];
            partOf = [ "${service_name script}.service" ];
            timerConfig = {
              OnCalendar = time;
              Unit = "${service_name script}.service";
              Persistent=true;
            };
          });

          # modify these
          scripts_path = "/build/src/JS/";
          scripts = {
            "subreddits.js"  = "*-*-* *:*:00";
          };

        in {
          options.services."${package_name}" = {
            enable = mkEnableOption "enable ${package_name}";

            config = mkOption rec {
              type = types.str;
              default = "./.env";
              example = default;
              description = "The env file";
            };

           # specific for teh program running
           prefix = mkOption rec {
              type = types.str;
              default = "silver_";
              example = default;
              description = "The prefix used to name service/folders";
           };

           user = mkOption rec {
              type = types.str;
              default = "${package_name}";
              example = default;
              description = "The user to run the service";
           };

          };

          config = mkIf cfg.enable {

            systemd.services = {
              "${cfg.prefix}${cfg.user}" = {
                description = "${description}";

                wantedBy = [ "multi-user.target" ];
                after = [ "network-online.target" ];
                wants = [ ];
                serviceConfig = {
                  # fill figure this out in teh future
                  DynamicUser=true;
                  Restart = "always";
                  ExecStart = "${nodejs}/bin/node ${root_dir}";
                  WorkingDirectory="${root_dir}";
                  EnvironmentFile = "${cfg.config}";
                };
              };

            # add the other services
            } // serviceGenerator scripts;

            # timers to run the above services
            systemd.timers = timerGenerator scripts;

          };

        };


    });
}
